
<meta charset="utf-8" />

# Hiroki SHIROKURA

<a href="./ja.html">Japanese Version is Here</a>

I'm interested in High Performance Networking.
Now, I reseach that how to exploit low-latency and high-throughput networking
with Software (e.g. DPDK) and Comodity hardwares.
I'm a 4th grader of the science and engineering department and join
Network-Security-Lab of HU.


- Fullname: Hiroki SHIROKURA
- Handlename: slankdev
- Countory: Japan
- Age     : 22 (B4)
- Email   : slank.dev [at] gmail.com



I also join the following events.

- Contributing DPDK
- IIJ-II Part time Job (Researching about Packet Processing)
- Cybozu Labs Youth
- IPA Security Camp (Tutor)



## Research Topics

- HPN (High Performance Netwoking) with Software
	- Dynamic Auto tuning of DPDK
	- Exploit library that is friendly DPDK



## SNS

- [GitHub](https://github.com/slankdev)
- [Blog](http://slankdev.hatenablog.com)
- [Slideshare](http://slideshare.net/slankdev)
- [Twitter](https://twitter.com/slankdev)
- [Facebook](https://facebook.com/hiroki.shirokura)



## Development

- [Cuishark: ](http://github.com/slankdev/cuishark)
  a protocol analyzer like a wireshark on CUI/TUI
- [LibPGEN: ](http://libpgen.org)
  extensible packet analyze library in C++11
- [STCP: ](https://github.com/slankdev/stcp)
  network stack using DPDK



## Publications

- High Performance Network stack using DPDK, Anual Report in CybozuLabYouth
  [link](https://www.slideshare.net/slankdev/dpdk-74027268)
- High Performance Networking with DPDK & Multi/Many Core
  [link](https://www.slideshare.net/slankdev/high-performance-networking-with-dpdk-multimany-core)
- Offloading BPF Implementation to FPGA-NIC (propose)
  [link](https://www.slideshare.net/slankdev/offloading-bpf-implementation-to-fpganic)
- Anual Report in Seccamp2017 as Tutor
  [link](https://www.slideshare.net/slankdev/seccamp-2016)
- LibPGEN: Extensible Packet analyze Library, Anual Report in CybozuLabYouth
  [link](https://www.slideshare.net/slankdev/ss-60291523)
- Development Packet Analyze Library in Security-Camp-Award 2016 (award of excellence)
  [link](https://www.slideshare.net/slankdev/seurity-camp-award-2016)

Some another slides. please see my [slideshare](http://slideshare.net/slankdev).


## Lectures

- 2017 Open Source Conference Hokkado (will)
- 2016 Open Source Conference Tokyo/Spring
- 2016 Open Source Conference Hokkaido
- 2016 IPA Security Camp


## Activity

- 2015 IPA Security Camp
- 2015 Open Source Conference Tokyo/Fall Show
- 2016 Open Source Conference Tokyo/Spring Lecture
- 2016 Open Source Conference Hokkaido Lecture
- 2016 IPA Security Camp Tutor
- 2016 Cybozu Labs Youth

Last update 2017.5.5


Copyright &copy; 2017-2020 Hiroki SHIROKURA All Rights Reserved.
